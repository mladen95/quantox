<?php
    require __DIR__ . '/core/bootstrap.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Qunatox - Register</title>
  </head>
  <body>
      <?php
        include __DIR__ . '/template/nav.php';
      ?>
      
      <h2>Register</h2>
      <?php
        if (isset($_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
      ?>
      <form action="controllers/register.php" method="post" class="hform">
            <p>
                <label>Name: </label>
                <input type="text" name="name" required="true">
            </p>
            <p>
                <label>Email: </label>
                <input type="text" name="email" required="true">
            </p>
            <p>
                <label>Password: </label>
                <input type="password" name="password" required="true">
            </p>
            <p>
                <label>Repeat password: </label>
                <input type="password" name="_password" required="true">
            </p>
            <input type="hidden" name="_csrf_token" value="<?= $_SESSION['_csrf_token'] ?>" >
        
            <p>
                <input type="submit" name="submit" value="Register" class="button">
            </p>
      </form>
  
  </body>
</html>