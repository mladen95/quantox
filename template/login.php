
<h2>Login</h2>
<?php
  $authUser = App\Users\User::getLoggedInUser();
  if ($authUser !== NULL)
      echo 'Welcome ' . $authUser->getName();

  if (isset($_SESSION['message'])){
      echo $_SESSION['message'];
      unset($_SESSION['message']);
  }
?>
<form action="controllers/login.php" method="post" class="hform">
      <p>
          <label>Email: </label>
          <input type="text" name="email" required="true">
      </p>
      <p>
          <label>Password: </label>
          <input type="password" name="password" required="true">
      </p>
      <input type="hidden" name="_csrf_token" value="<?= $_SESSION['_csrf_token'] ?>" >

      <p>
          <input type="submit" name="submit" value="Login" class="button">
      </p>
</form>
