<?php

namespace App\Users;

use App\DB\Database;
use App\Utils\Validation;
use App\Utils\Helper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Mladen
 */
class User {
    
    const TABLE_NAME = 'users';
    
    private $email;
    private $name;
    
    
    /**
     * Try to register user with given input, returns string if error is found in validation
     * 
     * @param String $email
     * @param String $name
     * @param String $password
     * @param String $_password
     * @return String
     */
    public static function register($email, $name, $password, $_password)
    {
        if (! Validation::checkEmail($email))
            return 'Email address format is invalid.';
        else if (! Validation::checkIsEmailUnique($email))
            return 'Email address already exists.';
        else if (! Validation::checkPasswordLength($password))
            return 'Password must be at least 8 characters long.';
        else if (! Validation::checkPasswordRepeat($password, $_password))
                return 'Passwords do not match.';
        
        $user = new User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword(Helper::createHash($password));
        $user->save();
    }
    
    
    /**
     * Try to log in user with given parameters
     * _token is saved in db and it's used for check if user is logged in
     * 
     * @param String $email
     * @param String $password
     * @return boolean
     */
    public static function login($email, $password)
    {
        $db = Database::getDB();
        $users = $db->read('SELECT * FROM `'.self::TABLE_NAME.'` WHERE `email`=?', [$email]);
        if (! count($users))
            return false;
        $user = $users[0];
        if (! Helper::checkPasswordHash($password, $user['password']))
                return false;
        
        $_SESSION['email'] = $user['email'];
        $_SESSION['name'] = $user['name'];
        $_SESSION['_token'] = Helper::generateRandomString(20);
        
        $db->execute('UPDATE `'.self::TABLE_NAME.'` SET `session_id`=?, `token`=? WHERE `id`=?', [session_id(), $_SESSION['_token'], $user['id']]);
        return true;
    }
    
    
    /**
     * Checks if user with _token and email in SESSION exists in database
     * 
     * @return \App\Users\User
     */
    public static function getLoggedInUser()
    {
        if (! isset($_SESSION['_token']) || ! isset($_SESSION['email']))
            return NULL;
        
        $user = Database::getDB()->read('SELECT * FROM `'.self::TABLE_NAME.'` WHERE `email`=? AND `token`=?', [$_SESSION['email'], $_SESSION['_token']]);
        if (! count($user))
            return NULL;
        
        $authUser = new User();
        $authUser->setEmail($user[0]['email']);
        $authUser->setName($user[0]['name']);
        return $authUser;
    }
    
    
    /**
     * Log out user
     */
    public static function logout()
    {
        session_destroy();
    }
    
    
    function getEmail() {
        return $this->email;
    }

    function getName() {
        return $this->name;
    }

    function getPassword() {
        return $this->password;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPassword($password) {
        $this->password = $password;
    }

        
    /**
     * Save user to db
     */
    public function save()
    {
        Database::getDB()->insert('INSERT INTO `'.self::TABLE_NAME.'` (`name`, `password`, `created_at`, `email`) VALUES (?,?,?,?)', [
            $this->name,
            $this->password,
            date('Y-m-d H:i:s'),
            $this->email
        ]);
    }
    
}
