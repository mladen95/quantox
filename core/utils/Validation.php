<?php

namespace App\Utils;

use App\DB\Database;
use App\Users\User;

/**
 * Description of Validate
 *
 * @author Mladen
 */
class Validation {
    
    /**
     * Checks if email address is in right format
     * 
     * @param String $email
     * @return Boolean
     */
    public static function checkEmail($email) : bool
    {
        if (empty($email))
            return false;
        else if (! filter_var($email, FILTER_VALIDATE_EMAIL))
            return false;
        else
            return true;
    }
    
    /**
     * Checks if repeated password mathces typed password
     * 
     * @param String $password
     * @param String $_password
     * @return Boolean
     */
    public static function checkPasswordRepeat($password, $_password) : bool
    {
        if ($password != $_password)
            return false;
        else
            return true;
    }
    
    /**
     * Checks if password length is longer or equal than 8 characters
     * 
     * @param String $password
     * @return Boolean
     */
    public static function checkPasswordLength($password) : bool
    {
        if (strlen($password) < 8)
            return false;
        else
            return true;
    }
    
    /**
     * Checks if typed email is unique in system
     * 
     * @param String $email
     * @return Boolean
     */
    public static function checkIsEmailUnique($email) : bool
    {
        $db = Database::getDB();
        $total = $db->read('SELECT COUNT(`id`) AS `total` FROM `'.User::TABLE_NAME.'` WHERE `email`=?', [$email])[0]['total'];
        return $total == 0;
    }
    
    
}
