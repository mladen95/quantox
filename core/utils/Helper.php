<?php



namespace App\Utils;

/**
 * Description of Helper
 *
 * @author Mladen
 */
class Helper {
    
    
    public static function createHash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    
    public static function checkPasswordHash($password, $hash)
    {
        return password_verify($password, $hash);
    }
    
    public static function generateRandomString($length = 30) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public static function safeEcho($text)
    {
        return htmlspecialchars($text, ENT_QUOTES);
    }
    
}
