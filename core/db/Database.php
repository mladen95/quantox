<?php

namespace App\DB;



/**
 * Description of Database
 *
 * @author Mladen
 */
class Database {
    const TIP_BAZE = 'MySQL';
    const CHARSET = 'utf8';

    private $hostname;
    private $username;
    private $password;
    private $dbname;
    private $charset;
    private $conn;
    
    /**
     * Global DB instance
     * @var App\DB\Database 
     */
    private static $dbInstance = NULL;
    
    private function __construct()
    {
        $host = constant('DB_HOST');
        $user = constant('DB_USER');
        $pass = constant('DB_PASSWORD');
        $db = constant('DB_NAME');
        $chset=self::CHARSET;
        
        $dsn = "mysql:host=$host;dbname=$db;charset=$chset";
        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

       
        $this->conn = new \PDO($dsn, $user, $pass, $opt);
        $this->hostname = $host;
        $this->username = $user;
        $this->password = $pass;
        $this->dbname = $db;
        $this->charset = $chset;

    }
    
    
    /**
     * Get DB instance
     * 
     * @return App\DB\Database
     */
    public static function getDB()
    {
        if (self::$dbInstance == NULL){
            try {
                self::$dbInstance = new Database();
            } catch(\PDOException $e){
                return NULL;
            }
        }
        return self::$dbInstance;
    }
    
    /**
     * Read from database
     * 
     * @param String $query
     * @param Array $params
     * @return Array
     */
    public function read($query, Array $params = NULL)
    {
        $result = $this->conn->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    /**
     * Execute query
     * 
     * @param String $query
     * @param Array $params
     * @return Boolean
     */
    public function execute($query, Array $params = NULL)
    {
        $result = $this->conn->prepare($query);
        return $result->execute($params);
    }

    /**
     * Insert into databse, returns inserted ID
     * 
     * @param String $query
     * @param Array $params
     * @return Int
     */
    public function insert($query, Array $params = NULL)
    {
        $result = $this->conn->prepare($query);
        $result->execute($params);
        return $this->conn->lastInsertId();
    }
    
}
