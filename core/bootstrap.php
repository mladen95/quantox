<?php

/**
 * This file is included on every page
 * it loads autoload and config file, and sets CSRF token
 */

require __DIR__ . '/../vendor/autoload.php';
session_start();
$_SESSION['_csrf_token'] = App\Utils\Helper::generateRandomString();

require __DIR__ . '/config.php';