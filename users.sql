-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 16, 2018 at 08:49 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quantox`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `password` text NOT NULL,
  `created_at` datetime NOT NULL,
  `email` text NOT NULL,
  `session_id` text,
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `created_at`, `email`, `session_id`, `token`) VALUES
(1, 'Mladen', '$2y$10$tXZR5n2xKInxGvC1jy6auu6Q7idQcZ1RCcn6Wzr75j.XyVrTJdSKC', '2018-08-16 22:34:22', 'mladen.simijonovic@gmail.com', 'qnt5p08i8oglvq8e71o75ad8jg', '3N7YmqPwIYyA4vRVWss9'),
(2, 'Petar', '$2y$10$RoIGdvoFF2nQyl2g3XMZSeVpNH7uQlQzNALI6TEQTG7L4/vrKgh6G', '2018-08-16 22:35:13', 'petar@test.com', 'qnt5p08i8oglvq8e71o75ad8jg', 'woz7gGUXlp1appKd7ZTO'),
(3, 'Tamara', '$2y$10$HqKm0tI4wqQKk1SJjNTV4uTd6C15KLzMVgYS80EbU4U4DS2w/dkKi', '2018-08-16 22:48:57', 'tamara@test.com', 'qnt5p08i8oglvq8e71o75ad8jg', '8RkTwA8W1MGWMPDUHvzV');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
