<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../core/config.php';

session_start();

// Only POST requests
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
   die('Error');
}

// Checks if request is comming from our webpage
if (! isset($_POST['_csrf_token']))
    die('CSRF token not found');

if ($_POST['_csrf_token'] !== $_SESSION['_csrf_token'])
    die('CSRF token do not match');


$result = App\Users\User::register($_POST['email'], $_POST['name'], $_POST['password'], $_POST['_password']);


if (empty($result))
    $_SESSION['message'] = 'OK';
else
    $_SESSION['message'] = $result;

header('Location: ../register.php');