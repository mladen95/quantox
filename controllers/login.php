<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../core/config.php';


session_start();

// Only POST requests
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
   die('Error');
}

// Checks if request is comming from our webpage
if (! isset($_POST['_csrf_token']))
    die('CSRF token not found');

if ($_POST['_csrf_token'] !== $_SESSION['_csrf_token'])
    die('CSRF token do not match');

// Try to log in user
$result = App\Users\User::login($_POST['email'], $_POST['password']);


if (! $result)
    $_SESSION['message'] = 'Error logging you in.';

header('Location: ../login.php');