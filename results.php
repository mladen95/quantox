<?php
    require __DIR__ . '/core/bootstrap.php';
    use App\Utils\Helper;

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Qunatox - Results</title>
  </head>
  <body>
      <?php
        include __DIR__ . '/template/nav.php';
      ?>
      
      <h2>Results</h2>
      <?php
      
        $authUser = App\Users\User::getLoggedInUser();
        
        if ($authUser !== NULL) {
        
            if (isset($_POST['query'])) {
                $db = App\DB\Database::getDB();
                $results = $db->read('SELECT * FROM `'. App\Users\User::TABLE_NAME.'` WHERE `email` LIKE ? OR `name` LIKE ?', [
                    '%'.$_POST['query'].'%',
                    '%'.$_POST['query'].'%'
                ]);

                if (count($results)) {
                    echo '<table>'
                        . '<thead>'
                            . '<tr>'
                                . '<th>Name</th>'
                                . '<th>Email</th>'
                            . '</tr>'
                        . '</thead>'
                        . '<tbody>';

                    foreach($results as $result){
                        echo '<tr>'
                        . '<td>'.Helper::safeEcho($result['name']).'</td>'
                        . '<td>'.Helper::safeEcho($result['email']).'</td>'
                                . '</tr>';
                    }
                    echo '</tbody></table>';
                }
                else
                    echo 'No results.';
            }
            
        }
        
        else {
            echo '<p>Please login.</p>';
            include __DIR__ . '/template/login.php';
        }
        
        
      ?>
      
  
  </body>
</html>