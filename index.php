<?php
    require __DIR__ . '/core/bootstrap.php';


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Qunatox - Home screen</title>
  </head>
  <body>
      <?php
        include __DIR__ . '/template/nav.php';
      ?>
      
      <h2>Home screen</h2>
      <form action="results.php" method="post" class="hform">
            <p>
                <label>Search: </label>
                <input type="text" name="query">
            </p>
            <input type="hidden" name="_csrf_token" value="<?= $_SESSION['_csrf_token'] ?>" >
        
            <p>
                <input type="submit" name="submit" value="Search" class="button">
            </p>
      </form>
      
  
  </body>
</html>